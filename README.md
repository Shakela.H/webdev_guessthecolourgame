# Colours Game
A simple colour guessing game 

## Description
A game that lets the player choose what color they would like to guess and then select the tiles they think has a dominant color matching their color to guess. If a score is above 0, they players' name and score get to be in the top 10 scores. Access cheat mode by pressing Shift+C and disable it by pressing Shift+C again. Player must enter their name, boardsize, color to guess and difficulty.

## Usage
Visit the application URL in your web browser.
https://sonic.dawsoncollege.qc.ca/~1435730/ColoursGame/

## Task distribution and contribution
Cheat-code.js: Shakela
Gameboard.js: Nathan
Highscore.js: Shakela
Main.js: Nathan
index.html: (primarily) Shakela, (fixes) Nathan
webpage.css: Shakela & Nathan
mobile.css: Shakela

Bug Check: Shakela & Nathan
Bug Fixes: Nathan

code improvement: Nathan



