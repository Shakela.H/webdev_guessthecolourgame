/* eslint-disable no-undef */

/**
 * @file manages flow of the game and other functions, switching 
 *       the page between the states of playing and not playing.
 * @author Nathan Bokobza
 */

"use strict";

let playing = false;

document.addEventListener("DOMContentLoaded", function() {
    
    /** 
     * changes button to correct colour when page loads
     */
    const setupButton = document.getElementById("setupButton");
    setupButton.style.backgroundColor = document.getElementById("colour").value;
    document.getElementById("colour").addEventListener("change", function(e) {
        setupButton.style.backgroundColor = e.target.value;
    });

    /**
     * Event listener for the button that will start the game.
     * Flips game states and calls the table creation.
     */
    document.forms[0].addEventListener("submit", function(e) {
        e.preventDefault();
        const gameColour = document.getElementById("colour").value;
        /**
         * Regenerates table until one is obtained with at least
         * one tile of the chosen colour.
         */
        do { 
            createTable(
                e.target.size.value, 
                Number(e.target.difficulty.value));
        } while (countColours(gameColour) === 0);

        /**
         * Generates message for player as to what their target
         * colour and tile count.
         */
        const info = document.getElementById("playingInfo").content.cloneNode(true);
        document.getElementById("coloursTable").appendChild(info);

        document.getElementById("tilesColour").textContent = gameColour.toLowerCase();
        document.getElementById("tilesTotal").textContent = countColours(gameColour);

        changeGameState();

        if (document.body.classList.contains("cheat")) {
            showCheat();
        }
        
        /**
         * Event listener for button that ends the game.
         * Flips game states and writes win/loss message
         */
        document.getElementById("endButton").addEventListener("click", function() {
            changeGameState();
            const messageNode = document.getElementById("colourMessage");
            messageNode.textContent = 
                `You got ${gameEndManager().numCorrect}
                correct out of ${countColours(gameColour)} ${gameColour.toLowerCase()} tiles!
                That's ${Math.floor(gameEndManager().numCorrect / countColours(gameColour) * 100)}%
                correct! `;
            if (getScore(...Object.values(gameEndManager())) > 0) {
                messageNode.textContent +=
                    `Your score was ${getScore(...Object.values(gameEndManager()))}, good job!`;
            } else {
                messageNode.textContent +=
                    `Your score wasn't over zero so better luck next time!`;
            }
        });
    });

    /**
     * Function that counts how many tiles of a given colour
     * there are on the gameboard.
     * @param {String} colour 
     * @returns total number of tiles of that colour
     */
    function countColours(colour) {
        let total = 0;
        const tdList = document.querySelectorAll("table td");
        for(let i = 0; i < tdList.length; i++) {
            const rgb = tdList[i].style.backgroundColor;

            if (getHigestColour(rgb) === colour) {
                total++;
            }
        }
        return total;
    }

    /**
     * Changes the state of they game by flipping global variable playing.
     * Depending on if playing is true or false, the interactibility
     * of each element in the function flips.
     *  if not playing: can't submit gameboard score
     *  if playing: can't edit player info or reset board
     */
    function changeGameState() {
        playing = !playing;
        document.getElementById("setupButton").disabled = playing;
        document.getElementById("player-name").disabled = playing;
        document.getElementById("size").disabled = playing;
        document.getElementById("colour").disabled = playing;
        document.getElementById("difficulty").disabled = playing;
        document.getElementById("endButton").disabled = !playing;
    }
});

/**
 * Retrieves the statistics from the game.
 * @returns an object containing:
 *          the number of correct guesses,
 *          the number of total guesses, 
 *          the size of the board,
 *          and the difficulty level of the game.
 */
function gameEndManager() {
    const numCorrect = countCorrect(document.getElementById("colour").value);
    const numSelected =  Array
        .from(document.querySelectorAll("td"))
        .reduce( (acc, cur) => acc + Number(cur.classList.contains("selected")), 0);
    const boardSize = Number(document.forms[0].size.value);
    const difficulty = Number(document.forms[0].difficulty.value);

    return {numCorrect, numSelected, boardSize, difficulty};
}

/**
 * Function that counts how many tiles a player
 * selected of the given colour.
 * @param {String} colour 
 * @returns total number of titles.
 */
function countCorrect(colour) {
    let total = 0;
    const tdList = document.querySelectorAll("table td");
    for(let i = 0; i < tdList.length; i++) {
        const rgb = tdList[i].style.backgroundColor;

        if (getHigestColour(rgb) === colour && tdList[i].classList.contains("selected")){
            total++;
        }
    }
    return total;
}

/**
 * @param {String} rgb 
 * @returns returns the highest colour value in given tile's rgb.
 */
function getHigestColour(rgb) {

    const red = 0;
    const green = 1;
    const blue = 2;
    rgb = rgb.substring(4, rgb.length - 1)
        .split(", ").map( x => Number(x));
    if (rgb[red] > rgb[green] && rgb[red] > rgb[blue]) {
        return "Red";
    } else if (rgb[green] > rgb[red] && rgb[green] > rgb[blue]) {
        return "Green";
    } else if (rgb[blue] > rgb[red] && rgb[blue] > rgb[green]) {
        return "Blue";
    }
}