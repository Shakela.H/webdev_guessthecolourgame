/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

/**
 * @file creates gameboard on button press using data in forms
 * @author Nathan Bokobza
 */

"use strict";

/**
 * Generates an RBG value which is dependant on game's
 * difficulty. Additionally makes sure that no two colour
 * values are the same.
 * @param {Number} difficulty 
 * @returns RGB value for a tile
 */
function pickColour(difficulty) {
    switch (difficulty) {
    case 0:
        difficulty = 255;
        break;
    case 1:
        difficulty = 80;
        break;
    case 2:
        difficulty = 40;
        break;
    case 3:
        difficulty = 10;
        break;
    default:
        break;
    }

    /**
     * @returns Random number between 0 and 255
     */
    const colourRNG = () => Math.floor(Math.random() * 255);

    /**
     * Validates that the colour generated is correct and 
     * also not a duplicate .
     * @param  {...Number} avoidedValues 
     * @returns 
     */
    const colourValidation = (...avoidedValues) => {

        let result;
        do {
            result = colourRNG();
        } while (
            result === avoidedValues[0] ||
            result === avoidedValues[1] ||
            Math.abs(avoidedValues[0] - result) > difficulty ||
            Math.abs(avoidedValues[1] - result) > difficulty);
        return result;
    };
    const r = colourRNG();
    const g = colourValidation(r);
    const b = colourValidation(g, r);

    return `rgb(${r},${g},${b})`;
}

/**
 * Creates gameboard and populates it with coloured tiles.
 * Replaces any pre-existing gameboard.
 * @param {Number} size 
 * @param {Number} difficulty 
 */
function createTable(size, difficulty) {
    const tableSlot = document.getElementById("coloursTable");
    tableSlot.textContent = undefined;
    const table = document.createElement("table");

    /**
     * Adds event listener on table to allow
     * tiles to be selected by user
     */ 
    table.addEventListener("click", function(e) {

        if (playing) {
            if (e.target.tagName === "TD"){
                if (e.target.classList.contains("selected")){
                    e.target.classList.remove("selected");
                } else{
                    e.target.classList.add("selected");
                }
            }
        }

        //Updates game message with how many tiles are selected.
        document.getElementById("tilesSelected").textContent = Array
            .from(document.querySelectorAll("td"))
            .reduce( (acc, cur) => acc + Number(cur.classList.contains("selected")), 0);
    
    });

    /**
     * Generates tiles into a table based
     * on size given by player
     */ 
    for (let i = 0; i < size; i++) {
        const tr = document.createElement("tr");
        for (let j = 0; j < size; j++) {
            const td = document.createElement("td");
            td.style.backgroundColor = pickColour(difficulty);
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    tableSlot.appendChild(table);
}