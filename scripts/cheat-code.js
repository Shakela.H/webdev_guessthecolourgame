/* eslint-disable no-undef */
/* eslint-disable prefer-const */
/**
 * @fileOverview This code implements a cheat-code functionality that 
 * reveals a hidden code when the SHIFT + C 
 * keys are pressed and reverts the display back to normal when the keys are pressed again.
 * @author Shakela Hossain
 */

"use strict";

/**
 * Listens for the keydown event and activates the cheat code when SHIFT + C keys are pressed.
 * @type {EventListener}
 */
document.addEventListener("keydown", (function () {
    let containColor = [];

    return function (e) {
        if (e.target.tagName.toLowerCase() !== "input" && e.shiftKey && e.keyCode === 67) {
            if (document.body.classList.contains("cheat")) {
                hideCheat(containColor);
                document.body.classList.remove("cheat");
            } else {
                containColor = showCheat();
                document.body.classList.add("cheat");
            }
        }
    };
})());

/**
 * Reveals the cheat code by displaying the background color and the 
 * highest color in the table cells.
 * @returns {Array} An array containing the cells and their colors.
 */
function showCheat() {
    const table = document.getElementsByTagName("table")[0];
    let containColor = [];
    table.querySelectorAll("tr").forEach(tr => {
        tr.querySelectorAll("td").forEach(td => {
            let rgb = td.style.backgroundColor;
            containColor.push({ td: td, color: rgb });
            td.textContent = rgb + "\n" + getHigestColour(rgb);
        });
    });
    return containColor;
}

/**
 * Hides the cheat code by clearing the content of the cells.
 * @param {Array} containColor - An array containing the cells and their original colors.
 */
function hideCheat(containColor) {
    containColor.forEach(entry => {
        entry.td.textContent = "";
    });
}