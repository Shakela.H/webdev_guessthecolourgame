/* eslint-disable prefer-const */
/* eslint-disable no-undef */
/**
 * @file Implementation of a highscore list table with 
 * sorting functionality and local storage support.
 * @author Shakela Hossain
 */

"use strict";

!(function() { 
    let isAscending = false;
    const numberOfScoresToShow = 10;
    let topScores = [];

    /**
    * Returns new DOM for a highscore list table row
    * @param {String} name Player name
    * @param {Number} score A score for a highscore list item
    * @returns row
    */
    function generateScoreRow(name, score) {
        const tr = document.createElement("tr");
        const tdName = document.createElement("td");
        const tdScore = document.createElement("td");

        tr.appendChild(tdName);
        tr.appendChild(tdScore);

        tdName.textContent = name && name.length ? name : "";
        tdScore.textContent = score;

        tdName.classList.add("name");
        tdScore.classList.add("score");

        return tr;
    }

    /**
     * Returns a JSON string representing the contents of the highscore list table.
     * @param {HTMLTableElement} table - The table element containing the highscore list.
     * @returns {String} - A JSON string representing the highscore list table.
     */
    function generateScoreTableJson(table) {
        const rows = Array.from(table.querySelectorAll("tr")).slice(1).map((row) => {
            const nameElement = row.querySelector(".name");
            const scoreElement = row.querySelector(".score");
            const name = nameElement ? nameElement.textContent : "";
            const score = scoreElement ? Number(scoreElement.textContent) : 0;
            return { name, score };
        });
        return JSON.stringify(rows);
    }

    document.addEventListener("DOMContentLoaded", function () {
        const body = document.getElementsByTagName("tbody")[0];
        const table = document.getElementById("score-list-table");
        const playerNameInput = document.getElementById("player-name");
        const highScoreSection = document.getElementById("highscore");
        const button = document.createElement("button");
        const storageKey = "scoreList";

        /**
        * adds a new score in the local storage.
        */
        function updateScore() { 
            const jsonData = generateScoreTableJson(table);
            localStorage.setItem(storageKey, jsonData);
        }

        /**
        * Listens for the click event. The player makes their guess and 
        * clicks on the button which in return we save the player's name and their score 
        * and creates a new row to add to the local storage
        * @type {EventListener}
        */
        document.addEventListener("click", function (e) {
            if (e.target && e.target.id === "endButton") {
                let playerName = playerNameInput.value;
                const newScore = gameEndManager();
                let score = getScore(...Object.values(newScore)); 
                if(score > 0){ 
                    let tableRow = generateScoreRow(playerName, score); 
                    body.appendChild(tableRow);
                }
                updateScore();
                renderScoreTable(); 
            }
        });

        /**
        * This will sort the score ascending or descending when toggled.
        */
        const toggleSortingOrder = function () {
            isAscending = !isAscending;
            renderScoreTable();
        };

        /**
        * Renders the score table. The row datas are retrived from the local 
        * storage and appends row in the highscore table.
        * this also limits the data saved in the local storage.
        */
        const renderScoreTable = function () {
            body.innerHTML = "";
            const jsonData = localStorage.getItem(storageKey);
            if (jsonData) {
                const data = JSON.parse(jsonData);
                if (isAscending) {
                    data.sort((a, b) => a.score - b.score);
                } else {
                    data.sort((a, b) => b.score - a.score);
                }
                topScores = data.slice(0, numberOfScoresToShow);
                topScores.forEach((item) => {
                    let tableRow = generateScoreRow(item.name, item.score);
                    body.appendChild(tableRow);
                });
            }

            /**
            * Creates a button if the array has values stored.
            */
            if(topScores.length !== 0){
                button.textContent = "Clear";
                button.setAttribute("id", "clear");
                highScoreSection.appendChild(button);
            }

            /**
            * Clears all the values from the local storage that contains that key.
            * @type {EventListener}
            */
            button.addEventListener("click", function() {
                localStorage.removeItem(storageKey); 
                topScores = [];
                renderScoreTable();
            });
        };

        /**
        * Listens for click event to toggle ascending/descending.
        * @type {EventListener}
        */
        document.querySelectorAll(".header")[1].addEventListener("click", toggleSortingOrder);
      
        renderScoreTable();
    });
}());

/**
 * Calculates the score based on various game parameters.
 * @param {Number} numCorrect - Number of correct answers.
 * @param {Number} numSelected - Number of selected items.
 * @param {Number} boardSize - Size of the game board.
 * @param {Number} difficulty - Difficulty level of the game.
 * @returns {Number} - The calculated score based on the provided parameters.
 */
function getScore(numCorrect, numSelected, boardSize, difficulty) {
    const percent = (2 * numCorrect - numSelected) / (boardSize * boardSize);
    return Math.floor(percent * 100 * boardSize * (difficulty + 1));
}